﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SlackIntegration
{
    public class Text
    {
        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        [JsonProperty("text", NullValueHandling = NullValueHandling.Ignore)]
        public string TextValue { get; set; }

        [JsonProperty("emoji", NullValueHandling = NullValueHandling.Ignore)]
        public bool Emoji { get; set; }
    }
}
