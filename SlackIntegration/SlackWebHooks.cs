﻿using log4net;
using Newtonsoft.Json;
using RestSharp;
using System;

namespace SlackIntegration
{
    public class SlackWebHooks
    {
        private RestClient restClient;
        private RestRequest restRequest;
        private ILog _logger;

        public SlackWebHooks(string url, ILog logger)
        {
            restClient = new RestClient();
            restClient.BaseUrl = new Uri(url);
            _logger = logger;
        }

        public void SendMessageToSlack(BaseTestResult testResult)
        {
            _logger.Info($"Send message to Slack");
            restRequest = new RestRequest(Method.POST);
            var body = JsonConvert.SerializeObject(testResult);
            restRequest.AddJsonBody(body);
            var requstResult = restClient.Execute(restRequest);
            if (!requstResult.IsSuccessful)
            {
                throw new Exception($"Request was not success due to reason '{requstResult.StatusDescription}'");
            }
            _logger.Info($"Message was sent Slack");
        }
    }
}
