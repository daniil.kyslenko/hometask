﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SlackIntegration
{
    public static class TestResultMessageFactory
    {
        public static BaseTestResult GetPositiveTestResult(string testCaseName, string testCaseId)
        {
            var result = GetBaseTestResult(testCaseName, testCaseId);
            result.Blocks[1].Fields.First().Text += "Success";
            return result;
        }

        public static BaseTestResult GetNegativeTestResult(string testCaseName, string testCaseId)
        {
            var result = GetBaseTestResult(testCaseName, testCaseId);
            result.Blocks[1].Fields.First().Text += "FAILED";
            return result;
        }

        private static BaseTestResult GetBaseTestResult(string testCaseName, string testCaseId)
        {
            var result = new BaseTestResult();
            result.Text = "Test Status";
            result.Blocks = new List<Block>();
            result.Blocks.Add(new Block() { Type = "header", Text = new Text() { Type = "plain_text", Emoji = true, TextValue = $"Test Case '{testCaseName}'. Id = {testCaseId}" } });
            result.Blocks.Add(new Block() { Type = "section", Fields = new List<Field>() { new Field() { Type = "mrkdwn", Text = "Status: " } } });
            result.Blocks.Add(new Block() { Type = "section", Fields = new List<Field>() { new Field() { Type = "mrkdwn", Text = $"When: {DateTime.Now.ToShortTimeString()}" } } });
            return result;
        }
    }
}
