﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SlackIntegration
{
    public class BaseTestResult
    {
        [JsonProperty("text", NullValueHandling = NullValueHandling.Ignore)]
        public string Text { get; set; }

        [JsonProperty("blocks", NullValueHandling = NullValueHandling.Ignore)]
        public List<Block> Blocks { get; set; }
    }
}
