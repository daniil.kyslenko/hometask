﻿using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SlackIntegration;
using System;
using System.Linq;
using UITesting.Pages;


namespace UITesting.Tests
{
    public class BaseTest : BasePage
    {
        protected SlackWebHooks slackWebHooks;
        protected string url = "https://demosite.executeautomation.com/";
        protected int waitingTime = 5;
        protected string urlSlackWebHook = "https://hooks.slack.com/services/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";

        [SetUp]
        public void Initialization()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(url);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(waitingTime);
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
            logger = Logger.Logger.GetLogger(typeof(BaseTest));
            slackWebHooks = new SlackWebHooks(urlSlackWebHook, logger);
        }

        [TearDown]
        public void TeardownTest()
        {
            driver.Quit();
            var testId = (string)TestContext.CurrentContext.Test?.Properties["Category"].FirstOrDefault();
            var testName = TestContext.CurrentContext.Test?.MethodName;
            if (TestContext.CurrentContext.Result.Outcome == ResultState.Error
                || TestContext.CurrentContext.Result.Outcome == ResultState.Failure)
            {
                slackWebHooks.SendMessageToSlack(TestResultMessageFactory.GetNegativeTestResult(testName, testId));
            }
            if (TestContext.CurrentContext.Result.Outcome == ResultState.Success)
            {
                slackWebHooks.SendMessageToSlack(TestResultMessageFactory.GetPositiveTestResult(testName, testId));
            }
        }

    }
}
