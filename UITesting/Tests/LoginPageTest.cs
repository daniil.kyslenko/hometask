﻿using NUnit.Framework;
using UITesting.Pages;

namespace UITesting.Tests
{
    [TestFixture]
    public class LoginPageTest : BaseTest
    {
        private readonly string _username = "username";
        private readonly string _password = "password";


        [Test]
        public void UserRegistration()
        {
            LoginPage loginPage = new LoginPage();

            loginPage.SetUserName(_username);
            loginPage.SetPassword(_password);
            Assert.IsTrue(loginPage.ElementIsClickable(loginPage.btnLogin));
            loginPage.ClickLoginButton();
        }
    }
}
