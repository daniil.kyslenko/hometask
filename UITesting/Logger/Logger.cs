﻿using log4net;
using log4net.Config;
using System;
using System.IO;

namespace UITesting.Logger
{
    public static class Logger
    {
        public static ILog GetLogger(Type t)
        {
            XmlConfigurator.Configure(new FileInfo("log4net.config"));
            return LogManager.GetLogger(t);
        }
    }
}
