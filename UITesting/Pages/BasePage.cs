using log4net;
using NLog;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace UITesting.Pages
{
    public static class BasePage
    {
        public static IWebDriver driver { get; set; }
        public static WebDriverWait wait { get; set; }
        public static ILog logger;

    }
}
