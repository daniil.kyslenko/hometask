﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;

namespace UITesting.Pages
{
    public class LoginPage : BasePage
    {
        public IWebElement UserNameField => driver.FindElement(By.XPath("//input[@name='UserName']"));
        public IWebElement PasswordField => driver.FindElement(By.XPath("//input[@name='Password']"));
        public IWebElement btnLogin => driver.FindElement(By.XPath("//input[@name='Login']"));

        public void SetUserName(string name)
        {
            logger.Debug($"Entering the value {name} into the UserName field");
            UserNameField.Clear();
            UserNameField.SendKeys(name);
            wait.Until(driver => UserNameField.Displayed);
        }

        public void SetPassword(string password)
        {
            logger.Info($"Entering the value {password} into the Password field");
            PasswordField.Clear();
            PasswordField.SendKeys(password);
            wait.Until(driver => PasswordField.Displayed);
        }

        public void ClickLoginButton()
        {
            logger.Info($"Click Login button and go to the main page");
            btnLogin.Click();
        }

        public bool ElementIsClickable(IWebElement element)
        {
           if(element != null && element.Displayed && element.Enabled) 
                return true;
           return false;
        }

    }
}
