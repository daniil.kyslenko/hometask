﻿using Atata;
using NUnit.Framework;


namespace AtataFrameworkIntegration.Tests
{
    [SetUpFixture]
    public class SetUITestSettings
    {

        protected string url = "https://skyup.aero/en/";
        protected string browser = "Chrome";

        [OneTimeSetUp]
        public void GlobalSetUp()
        {
            AtataContext.GlobalConfiguration
                .ApplyJsonConfig()
                .UseDriver(browser.ToLower())
                .UseBaseUrl(url)
                .UseAllNUnitFeatures()
                .AddScreenshotFileSaving()
                    .WithFolderPath(() => $@"Logs\{AtataContext.BuildStart:yyyy-MM-dd HH_mm_ss}")
                    .WithFileName(screenshotInfo => $"{AtataContext.Current.TestName} - {screenshotInfo.PageObjectFullName}")
                    .WithArtifactsFolderPath();

            AtataContext.GlobalConfiguration.AutoSetUpDriverToUse();
        }
    }
}
