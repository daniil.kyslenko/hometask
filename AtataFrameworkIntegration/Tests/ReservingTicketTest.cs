﻿using Atata;
using AtataFrameworkIntegration.Pages;
using NUnit.Framework;
using System.Linq;


namespace AtataFrameworkIntegration.Tests
{
    public class ReservingTicketTest:MainTestFixure
    {
        private static string departureCity = "KBP";
        private static string arrivalCity = "BER";


        [Test]
        public void Test()
        {
            ClosePopUps();

            var resultPage = Go.To<HomePage>()
                .DepartureCity.Click()
                .SearchPage.Set(departureCity)
                .ArrivalCity.Click()
                .SearchPage.Set(arrivalCity)
                .DepartureDate.Click()             
                .Days.Rows.First().Day.Wait(Until.Visible)
                .Days.Rows.First().Day.Click()
                .ReturnDate.Click()
                .Days.Rows.First().Day.Wait(Until.Visible)
                .Days.Rows.First().Day.Click()
                .Search.ClickAndGo();

            resultPage.DepartureTicket.Wait(Until.Visible)
                .DepartureTicket.Should.BeVisible()
                .ReturnTicket.Should.BeVisible();


        }
    }
}
