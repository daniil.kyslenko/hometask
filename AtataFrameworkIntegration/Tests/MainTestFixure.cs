﻿using Atata;
using AtataFrameworkIntegration.Pages;
using NUnit.Framework;

namespace AtataFrameworkIntegration.Tests
{
    [TestFixture]
    public class MainTestFixure
    {
        [SetUp]
        public void SetUp()
        {
            AtataContext.Configure().Build();
        }

        [TearDown]
        public void TearDown()
        {
            AtataContext.Current?.CleanUp();
        }

        protected HomePage ClosePopUps()
        {
            return Go.To<HomePage>()
                .CloseCovidPopup.Click()
                .AcceptCookiesPopup.Click();
        }
    }
}
