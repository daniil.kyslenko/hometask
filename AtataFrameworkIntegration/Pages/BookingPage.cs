﻿using Atata;


namespace AtataFrameworkIntegration.Pages
{
    using _ = BookingPage;
    public class BookingPage : Page<_>
    {
        [FindById("forwardFlight")]
        public Clickable<_> DepartureTicket { get; private set; }

        [FindById("backwardFlight")]
        public Text<_> ReturnTicket { get; private set; }
    }
}
