﻿using Atata;
using AtataFrameworkIntegration.Calendar;


namespace AtataFrameworkIntegration.Pages
{

    using _ = HomePage;
    
    public class HomePage : Page<_>
    {

        [FindByClass("modal__close js-close-modal")]
        public Button<_> CloseCovidPopup { get; private set; }

        [FindByClass("cookie-modal__btn")]
        public Button<_> AcceptCookiesPopup { get; private set; }

        [FindByXPath("//div[@id='deprtureCity']")]
        public Text<_> DepartureCity { get; private set; }

        [FindById("arrivalCity")]
        public Text<_> ArrivalCity { get; private set; }

        [FindById("forwardDateItem")]
        public Text<_> DepartureDate { get; private set; }

        [FindById("backwardDateItem")]
        public Text<_> ReturnDate { get; private set; }

        [FindById("citiesSearch")]
        [PressEnter]
        public SearchInput<_> SearchPage { get; private set; }

        [FindById("searchBtn")]
        public Button<BookingPage, _> Search { get; private set; }

        [FindById("citiesList")]
        public UnorderedList<ListItem<_>, _> ArrivalCities { get; private set; }

        [FindByClass("month1")]
        public Table<SelectingDay<_>, _> Days { get; private set; }
    }
}
