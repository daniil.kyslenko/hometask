﻿using Atata;


namespace AtataFrameworkIntegration.Calendar
{
    public class SelectingDay<_> : TableRow<_> where _ : Page<_>
    {
        [FindByClass("day toMonth valid tooltip-trigger")]
        public Text<_> Day { get; private set; }
    }
}
